import Vue from 'vue'
import Router from 'vue-router'

// Containers
import Full from '@/containers/Full'

// Views
import Dashboard from '@/views/Dashboard'
import Repository from '@/views/Repository'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '/dashboard',
      name: 'Home',
      component: Full,
      children: [
        {
          path: 'repository/:name',
          name: 'Repository',
          component: Repository,
          props: true
        },
        {
          path: 'dashboard',
          name: 'Repositories',
          component: Dashboard
        }
      ]
    }
  ]
})
